const express = require('express');
const { ApolloServer, gql } = require('apollo-server-express');
const mongoose = require('mongoose');

const app = express();
const port = process.env.PORT || 3000;

// MongoDB connection string
const mongoDBUri = 'mongodb+srv://elsie:munnamol0311@cluster0.iatt8fr.mongodb.net/';

// Connect to MongoDB (replace the connection string with your MongoDB URL)
mongoose.connect(mongoDBUri, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

// Check if the connection is successful
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));
db.once('open', () => {
  console.log('Connected to MongoDB');
});

// Define the State schema
const stateSchema = new mongoose.Schema({
  name: String,
});

// Create the State model
const State = mongoose.model('State', stateSchema);

// Function to insert states into MongoDB
const insertStates = async () => {
  const states = [
    'Alabama', 'Alaska', 'American Samoa', 'Arizona', 'Arkansas', 'California', 'Colorado', 'Connecticut', 'Delaware',
    'Florida', 'Georgia', 'Guam', 'Hawaii', 'Idaho', 'Illinois', 'Indiana', 'Iowa', 'Kansas', 'Kentucky',
    'Louisiana', 'Maine', 'Maryland', 'Massachusetts', 'Michigan', 'Minnesota', 'Mississippi',
    'Missouri', 'Montana', 'Nebraska', 'Nevada', 'New Hampshire', 'New Jersey', 'New Mexico',
    'New York', 'North Carolina', 'North Dakota', 'Northern Mariana Islands', 'Ohio', 'Oklahoma', 'Oregon', 'Pennsylvania',
    'Puerto Rico', 'Rhode Island', 'South Carolina', 'South Dakota', 'Tennessee', 'Texas', 'United States Virgin Islands', 'Utah', 'Vermont',
    'Virginia', 'Washington', 'West Virginia', 'Wisconsin', 'Wyoming',
  ];

  // Insert each state into the database
  for (const stateName of states) {
    const state = new State({ name: stateName });
    await state.save();
  }
};

// Call the insertStates function to populate the database
insertStates();

// GraphQL schema
const typeDefs = gql`
  type Query {
    searchStates(term: String!): [String]
  }
`;

// GraphQL resolvers
const resolvers = {
  Query: {
    searchStates: async (_, { term }) => {
      // Fetch states from MongoDB based on the search term
      const stateDocuments = await State.find({ name: { $regex: new RegExp(term, 'i') } });
      return stateDocuments.map((state) => state.name);
    },
  },
};

// Create Apollo Server
const server = new ApolloServer({ typeDefs, resolvers });

// Apply Apollo Server middleware to Express app
server.applyMiddleware({ app });

// Start the server
app.listen({ port }, () => {
  console.log(`Server ready at http://localhost:${port}${server.graphqlPath}`);
});
